import { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import  vCardsJS from 'vcards-js'

import { CardContainer } from "../components/Card"
import useFetchAndLoad from "../hooks/useFetchAndLoad"
import { user } from "../services/public.service";

import ContactIcon from '../assets/img/contact.svg'
import axios from "axios"

export const HomePage = () => {
  const { loader, callEndpoint } = useFetchAndLoad();

  const [base64, setbase64] = useState({
    base64Data: null,
  })

  const [userdata, setUserdata] = useState(null)
  let { id } = useParams();

  const getData = async () => {
    const userData = await callEndpoint(user(id));

    const { data } = userData;
    setUserdata(data);
  }

  const dowloadUser = () => {
    const reader = new FileReader();

    const { data } = userdata;
    const vCard = vCardsJS();

    vCard.firstName = data.nombre;
    vCard.lastName = data.apellido;
    vCard.organization = data.logo;
    vCard.workPhone = data.telefono?.replace(/ /g, "").slice(5);
    vCard.cellPhone = data.telefono?.replace(/ /g, "").slice(5);
    vCard.email = data.correo;
    vCard.title =  data.cargo;

    convertToBase64(data);
    console.log(vCard.getFormattedString());
    //vCard.photo.embedFromString(reader, 'image/jpeg');
    const url = window.URL.createObjectURL(new Blob([vCard.getFormattedString()]));
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', `${data.nombre + data.apellido}.vcf`);
    document.body.appendChild(link);
    link.click();
  }

  const convertToBase64 = (data) => {
    axios.get(
      `https://api.somosenelperu.com/${data.logo_id}`,
      {
        responseType: "arraybuffer",
      }
    )
    .then((response) =>{
    }
    );

  };
  
  useEffect(() => {
    getData()
    return () => {}
  }, [])
  
  return (
    <div className="container">
      <CardContainer user={userdata} />
      <button className='container-card__action'
      style={{background: userdata?.data?.color}}
      onClick={ ()=> { dowloadUser() }}>
        <img src={ContactIcon} alt="" />
        GUARDAR CONTACTO
      </button>
    </div>
  )
}
