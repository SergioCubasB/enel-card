import { Link } from 'react-router-dom';

import React from 'react'
import email from '../../assets/img/email.svg'
import whatsapp from '../../assets/img/whatsapp.svg'
import linke from '../../assets/img/linke.svg'
import webIcon from '../../assets/img/webicon.svg'
import ubicationIcon from '../../assets/img/ubicationIcon.svg'

import { ENVIRONMENT } from '../../environment/environment'


export const CardSocial = ({nombre, apellido, correo, telefono, linkedin, web, direccion, color}) => {
  return (
    <div className='container-card__social'>
      <ul>
        <li>
          <a onClick={() => window.location = `mailto:${correo}`}>
            <div className="icon" style={{backgroundColor: color}}>
              <img src={email} alt="" />
            </div>
            <p>{correo}</p>
          </a>
        </li>
        <li>
          <a href={`${ENVIRONMENT.API_WHATSAPP}${telefono?.replace('(', "").replace(')', "").replace('+', "").replace(/ /g, "")}`} target="_blank" rel="noreferrer">
            <div className="icon" style={{backgroundColor: color}}>
              <img src={whatsapp} alt="" />
            </div>
            <p>{telefono ? telefono : '-'}</p>
            </a>
        </li>
        <li>
          <a href={`${linkedin}`} target="_blank" rel="noreferrer">
            <div className="icon" style={{backgroundColor: color}}>
              <img src={linke} alt="" />
            </div>
            <p>{nombre} {apellido}</p>
          </a>
        </li>
        <li>
          <a href={`${web}`} target="_blank" rel="noreferrer">
            <div className="icon" style={{backgroundColor: color}}>
              <img src={webIcon} alt="" />
            </div>
            <p>{web}</p>
          </a>
        </li>
        <li>
          <a href={`${direccion}`} target="_blank" rel="noreferrer">
            <div className="icon" style={{backgroundColor: color}}>
              <img src={ubicationIcon} alt="" />
            </div>
            <p>Jr. Paseo del Bosque 500, San Borja</p>
          </a>
        </li>
      </ul>
    </div>
  )
}
