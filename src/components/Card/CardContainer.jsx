import React, { useEffect, useState } from 'react'

import { CardImage } from './CardImage'
import { CardInfo } from './CardInfo'
import { CardLogo } from './CardLogo'
import { CardSocial } from './CardSocial'

export const CardContainer = ({user}) => {
  return (
    <div className='container-card'>
      <CardImage image={user?.data?.logo_id} tipo={user?.data?.tipo} color={user?.data?.color}/>
      <CardInfo nombre={user?.data?.nombre} apellido={user?.data?.apellido} cargo={user?.data?.cargo}/>
      <CardSocial 
      nombre={user?.data?.nombre} 
      apellido={user?.data?.apellido} 
      correo={user?.data?.correo} 
      telefono={user?.data?.telefono} 
      linkedin={user?.data?.linkedin} 
      web={user?.data?.web} 
      direccion={user?.data?.direccion} 

      color={user?.data?.color}/>
    </div>
  )
}
