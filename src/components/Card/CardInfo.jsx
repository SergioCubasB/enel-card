import React, { useEffect } from 'react'

export const CardInfo = ( {nombre, apellido, cargo }) => {
  return (
    <div className='container-card__info'>
      <h1>{nombre} {apellido}</h1>
      <p>{cargo}</p>
    </div>
  )
}
