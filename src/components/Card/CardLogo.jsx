import React from 'react'
import logo from '../../assets/img/logo_x.png'
import enel_primary from '../../assets/img/enel-primary.png'
import enelxway from '../../assets/img/EnelXWay.png'
import EGP from '../../assets/img/EGP.png'
import cubo from '../../assets/img/logo_cuborojo.png'


export const CardLogo = ({tipo}) => {

  return (
    <>
      {(tipo === 1 &&
      <div className="container-card__logo" style={{right: '25px', bottom: '10px'}}>
          <img src={enel_primary} />
      </div>)

      || (tipo === 2 &&
        <div className="container-card__logo" style={{right: '15px', bottom: '10px'}}>
            <img src={logo} style={{width: '115px'}}/>
        </div>)

      || (tipo === 3 &&
        <div className="container-card__logo" style={{right: '10px'}}>
            <img src={EGP} />
        </div>)

      || (tipo === 4 &&
        <div className="container-card__logo" style={{right: '20px', bottom: '30px'}}>
            <img src={enelxway} />
        </div>)
      || (tipo === 5 &&
        <div className="container-card__logo" style={{right: '20px', bottom: '10px'}}>
            <img src={cubo} style={{width: '60px'}}/>
        </div>)
        }
    </>
  )
}
