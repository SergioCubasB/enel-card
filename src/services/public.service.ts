import axios from 'axios'
import { ENVIRONMENT } from '../environment/environment';

import { loadAbort } from "../utils/load-abort-axios.utility"

export const user = (id) => {
    const controller = loadAbort();

    return { call: axios.get<any>(`${ENVIRONMENT.DEV}usuario/${id}`, { signal: controller.signal }), 
            controller 
    }
}