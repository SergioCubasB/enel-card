import { Routes, Route } from 'react-router-dom'

import { HomePage } from '../pages'
import { NotFoundPage } from '../pages/NotFoundPage'

export const AppRouter = () => {
  return (
    <>
        <Routes>
            <Route path="usuario/:id"  element={ <HomePage /> } />
            <Route path="/*"    element={ <NotFoundPage /> } />
        </Routes>
    </>
    )
}
